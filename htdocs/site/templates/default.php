<?php snippet('head') ?>

<?php
foreach($page->merkur()->toBuilderBlocks() as $block):
  snippet('stacks/' . $block->_key(), array('data' => $block));
endforeach;
?>

<?php snippet('foot') ?>