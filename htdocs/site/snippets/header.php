<header class="sm:flex-no-wrap sm:pt-2 sm:justify-between absolute top-0 left-0 z-50 flex flex-wrap justify-around w-full px-4 pt-1 uppercase">
	<?php $menuItems = $site->Menu()->toStructure(); ?>
	<?php if($menuItems->count() >= 0): ?>
	<a href="<?= $menuItems->nth(0)->externallink() ?>"><?= $menuItems->nth(0)->linktitle() ?></a>
	<?php endif ?>

	<a class="sm:order-none sm:w-3/6 flex order-3 w-5/6" style="max-height: 40vh;" href="<?= $site->url() ?>"><img src="<?= $kirby->url('assets') ?>/images/haoshuai-paris.svg" alt="Haoshuai Paris logo"></a>
	<?php if($menuItems->count() >= 1): ?>
	<a href="<?= $menuItems->nth(1)->externallink() ?>"><?= $menuItems->nth(1)->linktitle() ?></a>
	<?php endif ?>
</header>