<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title><?= $site->title() ?></title>

	<?= css('/assets/css/style.css') ?>
</head>

<?php
$inlineStyle = [];
if($page->bgColorType() == 'manual' && $page->bgColor()->isNotEmpty()) {
	$inlineStyle[] = "background-color: " . $page->bgColor() . "; ";
}

if($page->textColorType() == 'manual' && $page->textColor()->isNotEmpty()) {
	$inlineStyle[] = "color: " . $page->textColor() . "; ";
}
$inlineStyle = implode(" ", $inlineStyle);
?>

<body class="" style="<?= $inlineStyle ?>">
<?php snippet('header') ?>