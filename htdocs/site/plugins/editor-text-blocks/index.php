<?php

/**
 * A block component should define a
 * matching snippet for the `blocks` field method.
 *
 * The snippet name must follow the scheme `editor/$type`
 *
 * If no snippet is defined, the block will be skipped when
 * the HTML is rendered in the template. This can be used
 * for block types that are only visible in the backend.
 */
Kirby::plugin('haoshuai/editor-text-blocks', [
    'snippets' => [
        'editor/textmega' => __DIR__ . '/snippets/textmega.php',
        'editor/break' => __DIR__ . '/snippets/break.php',
        'editor/textsmall' => __DIR__ . '/snippets/textsmall.php',
        'editor/textbig' => __DIR__ . '/snippets/textbig.php',
        'editor/button' => __DIR__ . '/snippets/button.php',
    ]
]);
