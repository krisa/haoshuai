<div class="button hover:scale-90 inline-block px-4 py-1 my-2 duration-500 transform bg-white rounded-full">
<?= $content ?>
</div>
