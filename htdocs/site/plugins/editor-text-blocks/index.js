/**
 * This is one of the most simple blocks you can build
 */
editor.block("textmega", {
  extends: "paragraph",

  // will appear as title in the blocks dropdown
  label: "Text mega",
  breaks: true,

  // icon for the blocks dropdown
  icon: "headline",
});

editor.block("textsmall", {
  extends: "paragraph",
  label: "Text small",
  breaks: true,
  icon: "text",
});
editor.block("textbig", {
  extends: "paragraph",
  label: "Text big",
  breaks: true,
  icon: "text",
});

editor.block("button", {
  extends: "paragraph",
  label: "Button",
  breaks: true,
  icon: "alert",
});

editor.block("break", {
  // will appear as title in the blocks dropdown
  label: "Empty line",
  icon: "circle-outline",


  // get the block content
  props: {
    content: String,
  },
  // block methods
  methods: {
    // the block must be focusable somehow
    // In this case we focus on the input.
    focus() {
      this.$refs.input.focus();
    },
    // The input event is sent to the editor
    // to update the block content
    onInput(event) {
      this.$emit("input", {
        content: event.target.value
      });
    }
  },
  // simple template. In single file components
  // this would be a bit nicer to read. You should
  // definitely go for single file components for more
  // complex blocks
  template: `
    <div class="linebreak-block">empty line</div>
  `,
});