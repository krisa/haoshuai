module.exports = {
	purge: [ // add more paths if you have templates/views somewhere else
		'./htdocs/site/templates/*.php',
		'./htdocs/site/templates/**/*.php',
		'./htdocs/site/snippets/*.php',
		'./htdocs/site/snippets/**/*.php',
		'./htdocs/site/snippets/**/*.php',
		'./htdocs/site/plugins/editor-text-blocks/snippets/*.php',
		'./htdocs/site/plugins/kirby-merkur/src/**/*.php',
		'./htdocs/site/plugins/kirby-merkur/src/layers/**/*.php',
		'./htdocs/site/plugins/kirby-merkur/src/stacks/**/*.php',
	],
	theme: {
		// screens: {
		// 	'sm': '640px',
		// 	'md': '768px',
		// 	'lg': '1024px',
		// 	'xl': '1280px',
		// },
		fontSize: {
			'xs': '.75rem',
			'sm': '.875rem',
			'tiny': '.875rem',
			'base': '1rem',
			'lg': '1.125rem',
			'xl': ['2rem', '1.2em'],
			// '2xl': '1.5rem',
			// '3xl': '1.875rem',
			// '4xl': '2.25rem',
			// '5xl': '3rem',
			// '6xl': '4rem',
			// '7xl': '5rem',
		},
		extend: {

		}
	}
}